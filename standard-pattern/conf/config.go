package conf

import (
	"log"

	"github.com/spf13/viper"
)

type AppConfig struct {
	Port      string `mapstructure:"PORT"`
	LogFormat string `mapstructure:"LOG_FORMAT"`
	DBDriver  string `mapstructure:"DB_DRIVER"`
	DBHost    string `mapstructure:"DB_HOST"`
	DBPort    string `mapstructure:"DB_PORT"`
	DBUser    string `mapstructure:"DB_USER"`
	DBPass    string `mapstructure:"DB_PASS"`
	DBName    string `mapstructure:"DB_NAME"`
	EnableDB  string `mapstructure:"ENABLE_DB"`

	SecretKey string `mapstructure:"SECRET_KEY"`
}

func LoadConfig(path string) (config AppConfig, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return AppConfig{}, err
	}

	err = viper.Unmarshal(&config)
	return AppConfig{}, nil
}

func LoadEnv() AppConfig {
	conf, err := LoadConfig("../../")
	if err != nil {
		log.Fatal("cannot load config: ", err)
	}
	return conf
}

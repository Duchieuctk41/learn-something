package repo

import (
	"standard-pattern/pkg/model"

	"gorm.io/gorm"
)

type Repo struct {
	Postgres *gorm.DB
}

type IRepo interface {
	CheckEmail(email string) (*model.Users, error)
	CreateUser(user *model.Users) (*model.Users, error)
	Promote(uRole *model.UserRoles) (*model.UserRoles, error)
	CheckUserRole(uRole *model.UserRoles) (*model.UserRoles, error)
}

func NewRepo(pg *gorm.DB) IRepo {
	repo := &Repo{
		Postgres: pg,
	}
	return repo
}

func (h *Repo) CheckEmail(email string) (*model.Users, error) {
	// check email existed
	// query email in database
	rs := &model.Users{}
	if err := h.Postgres.Where("email = ?", email).Preload("UserRoles").First(rs).Error; err != nil {
		// 2 case
		// case 1:
		// error not found => email not existed
		// case 2:
		// database err -> can not connect to db
		return nil, err
	}
	return rs, nil
}

func (h *Repo) CreateUser(user *model.Users) (*model.Users, error) {
	if err := h.Postgres.Create(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (h *Repo) Promote(uRole *model.UserRoles) (*model.UserRoles, error) {
	if err := h.Postgres.Create(uRole).Error; err != nil {
		return nil, err
	}
	return uRole, nil
}

func (h *Repo) CheckUserRole(uRole *model.UserRoles) (*model.UserRoles, error) {
	rs := &model.UserRoles{}

	if err := h.Postgres.Where("user_id = ? AND role_id = ?", uRole.UserID, uRole.RoleID).First(rs).Error; err != nil {
		return nil, err
	}

	return rs, nil
}

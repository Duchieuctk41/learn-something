package middleware

import (
	"net/http"
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/service"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type MiddleHandler struct {
	AuthSrv service.IAuthService
}

func NewMiddleHandler(auth service.IAuthService) *MiddleHandler {
	return &MiddleHandler{
		AuthSrv: auth,
	}
}

func (h *MiddleHandler) IsAuthenticated() gin.HandlerFunc {
	return func(c *gin.Context) {
		// get header Authorization, call service to get user
		req := model.Authorization{}
		c.BindHeader(&req)

		if req.TokenHeader == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, "unauthenticated")
			return
		}

		splitted := strings.Split(req.TokenHeader, " ") // Bearer jwt_token
		if len(splitted) != 2 {
			c.AbortWithStatusJSON(http.StatusForbidden, "unauthenticated bearer")
			return
		}

		_, err := h.AuthSrv.ParseJWTToken(splitted[1])
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
			return
		}

		c.Next()
	}
}

type Claims struct {
	Email       string   `json:"email"`
	DisplayName string   `json:"displayName"`
	Permissions []string `json:"permissions"`
	jwt.StandardClaims
}

func (h *MiddleHandler) CheckAdmin() gin.HandlerFunc {
	return func(c *gin.Context) {
		// get header Authorization, call service to get user
		req := model.Authorization{}
		c.BindHeader(&req)

		if req.TokenHeader == "" {
			c.AbortWithStatusJSON(http.StatusForbidden, "unauthenticated")
			return
		}

		splitted := strings.Split(req.TokenHeader, " ") // Bearer jwt_token
		if len(splitted) != 2 {
			c.AbortWithStatusJSON(http.StatusForbidden, "unauthenticated bearer")
			return
		}

		claims, err := h.AuthSrv.ParseJWTToken(splitted[1])
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, err.Error())
			return
		}

		isAdmin := false
		for _, v := range claims.Permissions {
			if v == "1" || v == "2" {
				isAdmin = true
			}
		}

		if !isAdmin {
			c.AbortWithStatusJSON(http.StatusForbidden, "unauthenticated bearer")
			return
		}

		c.Next()
	}
}

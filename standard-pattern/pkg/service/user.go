package service

import (
	"fmt"
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/repo"
	"standard-pattern/pkg/utils"

	"golang.org/x/crypto/bcrypt"
)

type Service struct {
	Repo repo.IRepo
}

type IService interface {
	SignUp(uReq *model.UserRequest) (*model.Users, error)
	CheckUserPassword(email string, password string) (*model.Users, error)
	GetUser(email string) (*model.Users, error)
	Promote(uRole *model.UserRoles) (*model.UserRoles, error)
}

func NewService(repo repo.IRepo) IService {
	return &Service{
		Repo: repo,
	}
}

func (h *Service) SignUp(uReq *model.UserRequest) (*model.Users, error) {
	err := utils.CheckConfirmPassword(uReq.Password, uReq.ConfirmPassword)
	if err != nil {
		return nil, err
	}

	user, err := h.Repo.CheckEmail(uReq.Email)
	if err != nil && !utils.IsErrNotFound(err) {
		return nil, err
	}
	if user != nil {
		return nil, fmt.Errorf("email existed")
	}

	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(uReq.Password), 14)

	newUser := &model.Users{
		DisplayName: uReq.DisplayName,
		Email:       uReq.Email,
		Password:    hashedPassword,
	}

	user, err = h.Repo.CreateUser(newUser)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (h *Service) CheckUserPassword(email string, password string) (*model.Users, error) {
	// get user by email
	user, err := h.Repo.CheckEmail(email)
	if err != nil {
		return nil, err
	}
	// check request password == user.pwd
	if err := bcrypt.CompareHashAndPassword(user.Password, []byte(password)); err != nil {
		return nil, fmt.Errorf("wrong password")
	}

	return user, nil
}

func (h *Service) GetUser(email string) (*model.Users, error) {
	user, err := h.Repo.CheckEmail(email)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (h *Service) Promote(uRole *model.UserRoles) (*model.UserRoles, error) {
	userRole, err := h.Repo.CheckUserRole(uRole)
	if err != nil && !utils.IsErrNotFound(err) {
		return nil, err
	}
	if userRole != nil {
		return nil, fmt.Errorf("user of role existed")
	}

	userRole, err = h.Repo.Promote(uRole)
	if err != nil {
		return nil, err
	}

	return userRole, nil
}

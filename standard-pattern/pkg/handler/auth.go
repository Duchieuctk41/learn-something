package handler

import (
	"net/http"
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/service"

	"gitlab.com/goxp/cloud0/ginext"
)

type AuthHandler struct {
	UserSrv IUser
	AuthSrv service.IAuthService
}

func NewAuthHandler(usr IUser, auth service.IAuthService) *AuthHandler {
	return &AuthHandler{
		UserSrv: usr,
		AuthSrv: auth,
	}
}

type IUser interface {
	CheckUserPassword(email string, password string) (*model.Users, error)
}

func (h *AuthHandler) Login(c *ginext.Request) (*ginext.Response, error) {
	// Take request, call service to handle logic
	req := model.LoginRequest{}
	c.MustBind(&req)

	// check email, password
	user, err := h.UserSrv.CheckUserPassword(req.Email, req.Password)
	if err != nil {
		return nil, ginext.NewError(http.StatusUnauthorized, err.Error())
	}

	// gen JWT
	token, err := h.AuthSrv.GenJWTToken(user)
	if err != nil {
		return nil, ginext.NewError(http.StatusInternalServerError, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, token), nil
}

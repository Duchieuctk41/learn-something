package handler

import (
	"net/http"
	"standard-pattern/pkg/model"
	"standard-pattern/pkg/service"

	"gitlab.com/goxp/cloud0/ginext"
)

type Handler struct {
	Service service.IService
}

func NewHandler(srv service.IService) *Handler {
	return &Handler{
		Service: srv,
	}
}

func (h *Handler) Register(c *ginext.Request) (*ginext.Response, error) {
	// parse body request, call service to handle logic
	req := model.UserRequest{}

	c.MustBind(&req)

	// call service
	rs, err := h.Service.SignUp(&req)
	if err != nil {
		return nil, ginext.NewError(http.StatusBadRequest, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, rs), nil
}

func (h *Handler) GetUser(c *ginext.Request) (*ginext.Response, error) {
	return ginext.NewResponseData(http.StatusOK, "success"), nil
}

func (h *Handler) Promote(c *ginext.Request) (*ginext.Response, error) {
	req := model.UsrRRequest{}
	c.MustBind(&req)

	userRoles := make([]model.UserRoles, len(req.Permissions))

	for i, roleID := range req.Permissions {
		userRoles[i] = model.UserRoles{
			UserID: req.UserID,
			RoleID: roleID,
		}
		_, err := h.Service.Promote(&userRoles[i])
		if err != nil {
			return nil, ginext.NewError(http.StatusBadRequest, err.Error())
		}
	}

	return ginext.NewResponseData(http.StatusOK, req), nil
}
